
* MP3 playeris maitinasi nuo 5V
* Stiprintuvas maitinasi nuo 3.3V ir turi didelės galios 14V maitinimą
* Stiprintuvas turi atskirą shutdown ir mute piną
* Ant I2C buso kabantys sensoriai maitinasi nuo 3.3V
* Ant MCP23017 expanderio kabo keli sensoriai, turi būti 4pinų jungtys
  * 5V
  * 3,3V
  * input - apsaugoti nuo viršįtampio
  * GND
* Visas į sensorius einantis 5V railas ir 3,3V railas apsaugotas polyfuse
  * Tarkim, kiekvienam sensoriui po 100mA, bet patikrinti, kiek valgo Teensis
* Teensį prijungti prie I2C buso ir padaryti galimybę prijungti prie UARTo per 0ohm jumperius

##Aidas
* Reikia prijungti mikrofoną prie teensio
* Galbūt perduoti audio per baby monitorių ar kokį nors Baofengą
* Galbūt naudoti FM mikrofoną

##Maitinimas
 * Keturios Li-FePO4 celės su apsauga 11.2V - 14V
 * apsaugai naudoti kažką tokio:  [](http://www.batteryspace.com/pcbprotectioncircuitmodulefor4cells128vlifepo3batterypackat7aimit.aspx)
 * Buck iki 5V, 2A max
 * Buck iki 3,3V, 2A max
 * ESP valgo 250mA
 * MP3 playeris 160mA max
 * Teensis - 200mA
 * Teensio audioboardas - max 70mA



 * Adafruito plokštę reikės šiek tiek modifikuoti, nes I2C išėjimai yra pajungti prie VDD, kas yra aukšta įtampa (14V)
 * Adafruito plokštė turi vidinį 3,3V reguliatorių, todėl ją maitinti tik aukšta įtampa. DĖMESIO! viduje stovintčio reguliatoriaus Vin max yra 16V

